Software Studio 2019 Spring Midterm Project
Topic
•	Project Name : Talk! (Chat room)
•	Key functions (add/delete)
1.	Chat with all the users that has an account in Talk!
2.	Chat with individual users
3.	Chat with Creator
4.	Load Previous messages

•	Other functions (add/delete)
1.	Change your user id from emails to other nicknames
2.	Shows chat history with timeline
3.	Different chat bubbles to differentiate from current user and other users
4.	Prevents users from swearing!!!!! Swearing is not good ☹

Basic Components
Component	Score	Y/N
Membership Mechanism	20%	Y
Firebase Page	5%	Y
Database	15%	Y
RWD	15%	Y
Topic Key Function	15%	Y

Advanced Components
Component	Score	Y/N
Third-Party Sign In	2.5%	Y
Chrome Notification	5%	Y
Use CSS Animation	2.5%	Y
Security Report	5%	Y

Website Detail Description
作品網址：https://midterm-project-41201.firebaseapp.com/signin.html

Components Description :
1.	Membership Mechanism : Logs in with a username and password
2.	Firebase page: https://midterm-project-41201.firebaseapp.com/signin.html
3.	Database: Records all users that have logged into Talk!, which is used to make individual chatrooms. When users submit messages, it records it with date, time, data, username and email. 
4.	RWD: Webpage is designed to be able to read in all different kinds of devices, which the variables won’t be flying around in smaller devices.
5.	Topic key function: Talk! Is able to load chat history for global chat and Private chats from the dropdown-menu on the top, where individual chats can be created by typing a users email or newest user id. The chat history is recorded in both users uid combined, where the uid is sorted so that both users can see the same chat history.
6.	Third-Party Sign In: User is able to sign in with google in the sign in page.
7.	Chrome notification: When a message is sent, there will be a chrome notification where it tells you rather the message came from the group chat or form an individual chatroom.
8.	CSS animation: In the sign in page, the “說話啊”.jpg flys from the top of the page to on top of the “Talk!” box, and it shakes!!
9.	Security report: See below.
Other Functions Description(1~10%) :
1.	Changing ID: If the user doesn’t want to use his/her email as his/her id, he/she can choose to change his/her id in the “edit profile” part under “Account”.
2.	Preventing Swearwords: If the user tries to swear in the chat, either in Chinese or English, the user might get kicked out of the page since Swearing is not allowed anywhere ☹.
3.	
Security Report (Optional)
When users submit a message, it checks if it’s a html/js code. If it’s so, it removes the tag that makes it a coding line and turns it into a simple text, therefore the users can’t easily make changes to the chatroom.
