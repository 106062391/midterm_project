function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    //var provider = new firebase.auth.FacebookAuthProvider();
    //var provider = new firebase.auth.GoogleAuthProvider();

    btnLogin.addEventListener('click', function () {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).catch(function(error) {
            // Handle Errors here.
            console.log("hi");
            var errorCode = error.code;
            var errorMessage = error.message;
            create_alert("error",errorMessage);
            txtEmail.value="";
            txtPassword.value="";
            
            }).then(function(success){
                create_alert("success",success.message);

                txtEmail.value="";
                txtPassword.value="";
                window.location="index.html";
            })
    });

    /*firebase.auth().signInWithPopup(provider).then(function(result) {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        var token = result.credential.accessToken;
        // The signed-in user info.
        var user = result.user;
        }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        })*/


        btnGoogle.addEventListener('click', function () {

            var provider = new firebase.auth.GoogleAuthProvider();
            firebase.auth().signInWithPopup(provider).then(function(result) {
                
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
                // The signed-in user info.
                var user = result.user;
                // ...
                }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;
                console.log(errorMessage);
                });
        });
    

    btnSignUp.addEventListener('click', function () {

        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(success) {
            create_alert("success",success.message);

            txtEmail.value="";
            txtPassword.value="";
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            create_alert("error",errorMessage);
            txtEmail.value="";
            txtPassword.value="";
        })

    });

}


// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};

/*  <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required> 
*/


/*
@keyframes fastSlide {
  0%   { transform: translate(1000%)   skewX(-30deg) ; }
  70%  { transform: translate(75%)   skewX(-30deg) ; }
  80%  { transform: translate(75%)   skewX(20deg)  ; }
  95%  { transform: translate(75%)   skewX(-10deg) ; }
  100% { transform: translate(75%)   skewX(0deg)   ; }
}
*/