function sort(a, b){
    if(a > b) return a+b;
    else return b+a;
}

function init() {
    var Username = '';

    var user_email = '';
    
    var user_uid;
    var user_id;

    var chatroom_uid;

    var private_mode = false;
    var public_mode = false;


    var str_after_content = "</p></div></div>\n";

    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');

        if (user) {
            user_email = user.email;
            Username = user.email;
            user_uid = user.uid;

            var Userlist = firebase.database().ref('userlist');
            var newAccount = true;

            Userlist.on('value', function (snapshot){
                snapshot.forEach(function (childSnapshot){
                    var value = childSnapshot.val();
                    if(user.email == value.email){
                        newAccount = false;
                    }
                })
                if(newAccount == true){
                    firebase.database().ref('userlist').push().set({
                        email: user.email,
                        userid: user.email,
                        useruid: user.uid
                    });
                }
            })


            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='setting'>Edit profile</span><span class='dropdown-item' id='logout-btn'>Logout</span>";

            var btnProfile = document.getElementById('setting');
            btnProfile.addEventListener('click', function(){
                Username = prompt("New Username?", "");

                var user = firebase.auth().currentUser;
                var changeref = firebase.database().ref(chatroom_uid);

                changeref.on('value', function (snapshot){
                    snapshot.forEach(function(childSnapshot){
                        var value = childSnapshot.val();

                        if(user.email == value.email){
                            user.Userlist.updateProfile({
                                userid: Username
                            }).then(function(){
                                console.log('success');
                            }).catch(function(error){
                                console.log(error.messege);
                            })
                        }
                    })
                })

            });

            var btnLogout= document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut()
                .then(function(messege){
                    console.log("logouts");
                })
                .catch(function(error){
 
                });
            });

        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "Woops! Looks like you haven't logged in yet!";
            document.getElementById('post_list').setAttribute("style", "text-align:center; font-size: 50px; color:red; background-image: url('img/loginplz.jpg'); background-repeat: no-repeat; background-size: 50%; background-position-x: 50%; background-position-y: 50%;");
        }
    });

    document.getElementById('Talk!').addEventListener('click', function(){
        if(confirm("說話啊????????????????????????"))window.location="https://www.google.com/search?q=%E8%AC%9B%E8%A9%B1%E5%95%8A&source=lnms&tbm=isch&sa=X&ved=0ahUKEwir29aro_zhAhVBVrwKHYo5BpoQ_AUIDigB&biw=1280&bih=650";
    })

    var Private = document.getElementById('Users');
    var Group = document.getElementById('Group');

    Private.addEventListener('click', function(){
        var Target = prompt("Who do you want to talk to?", "Type a username, or message me @ 'chengyuisme@gmail.com'");

        if(Target != null){
            var Userlist = firebase.database().ref('userlist');

            Userlist.on('value', function (snapshot){
                snapshot.forEach(function (childSnapshot){
                    var value = childSnapshot.val();

                    if(Target == value.email || Target == value.userid){
                        var chatroom = sort(value.useruid, user_uid);
                        console.log(chatroom);
                        chatroom_uid = chatroom;
                        public_mode = false;
                        private_mode = true;
                        
                        document.getElementById('topic').innerHTML = "Private chat room with " + Target;

                        var postsRef = firebase.database().ref(chatroom_uid);

                        var total_post = [];
                
                        postsRef.on('value', function (snapshot) {
                            total_post = [];
                            snapshot.forEach(function (childSnapshot) {
                                var value = childSnapshot.val();
                                if(user_email == value.email){
                                    total_post.push( "<p id='Ownuser'>"+value.userid + "  "+value.time +"</p>"+ "</strong>" +"</p>\n"+ "<p id='Ownspeechbubble'>"+ value.data +"</p>" + str_after_content);
                                }
                                else{    
                                    total_post.push( "<p id='user'>"+value.userid + "  "+value.time +"</p>"+ "</strong>" +"</p>\n"+ "<p id='speechbubble'>"+ value.data +"</p>" + str_after_content);
                                }
                            });
                            document.getElementById('post_list').setAttribute("style", "text-align:left; font-size: 20px;)");
                            document.getElementById('post_list').innerHTML = total_post.join(' ');
                        })
                    }
                })


            })
        }
    })

    Group.addEventListener('click', function(){
        private_mode = false;
        public_mode = true;

        document.getElementById('topic').innerHTML = "Global Chatroom";
        document.getElementById('topic').setAttribute('style', 'right:20px');

        var postsRef = firebase.database().ref('com_list');

        var total_post = [];

        postsRef.on('value', function (snapshot) {
            total_post = [];
            snapshot.forEach(function (childSnapshot) {
                var value = childSnapshot.val();
                if(user_email == value.email){
                    total_post.push( "<p id='Ownuser'>"+value.userid + "  "+value.time +"</p>"+ "</strong>" +"</p>\n"+ "<p id='Ownspeechbubble'>"+ value.data +"</p>" + str_after_content);
                }
                else{    
                    total_post.push( "<p id='user'>"+value.userid + "  "+value.time +"</p>"+ "</strong>" +"</p>\n"+ "<p id='speechbubble'>"+ value.data +"</p>" + str_after_content);
                }
            });
            document.getElementById('post_list').setAttribute("style", "text-align:left; font-size: 20px;");
            document.getElementById('post_list').innerHTML = total_post.join(' ');
        })
    })

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function(){
        if (post_txt.value != "") {
            var StrippedTxt = post_txt.value.replace(/<[^>]*>/g, '');
            var badword = false;
            if(StrippedTxt == 'fuck' || StrippedTxt == 'FUCK' || StrippedTxt == 'fuck u' || StrippedTxt == 'fuck you' || StrippedTxt == '幹' || StrippedTxt == '操' || StrippedTxt == '幹你娘' || StrippedTxt == '媽的'){
                alert("don't do that again");
                window.location="https://www.ddm.org.tw/page_view.aspx?siteid=&ver=&usid=&mnuid=1236&modid=59&mode=pc";
                badword = true;
            };
            var time = new Date();
            var hours = time.getHours();

            var month = time.getMonth()+1;
            var date = time.getDate();
            var year = time.getFullYear();

            var minutes = time.getMinutes();
            if(minutes%10 == minutes) minutes = "0"+minutes;
        
            CurTime = year+"/"+month+"/"+date+" "+hours+":"+minutes;
            console.log(CurTime);
            if(public_mode == true && badword == false){
                Push.create("Talk!",{
                    body: "New Global message! come back and take a look!",
                    icon: 'img/talk.jpg',
                    timeout: 5,
                    onClick: function () {
                        window.focus();
                        this.close();
                    }
                });
                firebase.database().ref('com_list').push().set({
                    email: user_email,
                    userid: Username,
                    data: StrippedTxt,
                    time: CurTime
                });
            }
            else if(private_mode == true && badword == false){
                Push.create("Talk!",{
                    body: "New Private message! come back and take a look!",
                    icon: 'img/talk.jpg',
                    timeout: 5,
                    onClick: function () {
                        window.focus();
                        this.close();
                    }
                });
                firebase.database().ref(chatroom_uid).push().set({
                    email: user_email,
                    userid: Username,
                    data: StrippedTxt,
                    time: CurTime
                });
            }
        }
        post_txt.value = "";
    });

    //var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";

    document.getElementById('post_list').innerHTML = "Pick your mode! AND PLEASE REMEMBER, DO NOT SWEAR :)";
    document.getElementById('post_list').setAttribute("style", "text-align:center; font-size: 50px;");
}

window.onload = function () {
    init();
};

/*background-image: url("../img/sakura.jpg");
  background-repeat: no-repeat;
  background-size: 100%;*/